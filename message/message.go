package message

import (
	"errors"
	"github.com/ethereum/go-ethereum/log"
	"github.com/ethereum/go-ethereum/rlp"
	"io"
)

const (
	maxUint24  = ^uint32(0) >> 8
	headerSize = 6

	Ping     = 0x01
	Pong     = 0x02
	Register = 0x03

	GetPeerInfo = 0x04
	PeerInfo    = 0x05
)

var (
	headerMagic = []byte{0xC2, 0xFE, 0x55}
)

type Msg struct {
	Code    uint64
	Content string
}

type StreamRW struct {
	conn io.ReadWriter
}

func NewStreamRW(conn io.ReadWriter) *StreamRW {
	return &StreamRW{conn}
}

func (s *StreamRW) ReadMsg() (*Msg, error) {
	var msg Msg
out:
	for {
		headBuf := make([]byte, headerSize)
		if _, err := io.ReadFull(s.conn, headBuf); err != nil {
			return nil, err
		}

		// Check the message header
		mSize := readInt24(headBuf)
		if err := headerCheck(headBuf); err != nil {
			// Discard the fake message
			continue
		}

		// Read the message content
		mBuf := make([]byte, mSize)
		if _, err := io.ReadFull(s.conn, mBuf); err != nil {
			return nil, err
		}

		// Decode the message
		if err := rlp.DecodeBytes(mBuf, &msg); err != nil {
			log.Debug("Message decode error: %v", err)
			continue
		}

		break out
	}

	return &msg, nil
}

func (s *StreamRW) WriteMsg(msg *Msg) error {
	if msg == nil {
		return errors.New("Can not send nil message ")
	}

	bMsg, err := rlp.EncodeToBytes(msg)
	if err != nil {
		log.Debug("RLP encode error: %v", err)
		return err
	}

	// Message header
	headBuf := make([]byte, headerSize)
	mSize := uint32(len(bMsg))
	if mSize > maxUint24 {
		return errors.New("message size overflows uint24")
	}
	putInt24(mSize, headBuf)
	copy(headBuf[3:], headerMagic)
	if s.conn.Write == nil {
		log.Debug("Invalid write interface ")
		return errors.New("Invalid write interface ")
	}

	if _, err := s.conn.Write(headBuf); err != nil {
		return err
	}

	_, err = s.conn.Write(bMsg)
	return err
}

func putInt24(v uint32, b []byte) {
	b[0] = byte(v >> 16)
	b[1] = byte(v >> 8)
	b[2] = byte(v)
}

func readInt24(b []byte) uint32 {
	return uint32(b[2]) | uint32(b[1])<<8 | uint32(b[0])<<16
}

//{0xC2, 0xFE, 0x55}
func headerCheck(h []byte) error {
	if h[3] == headerMagic[0] && h[4] == headerMagic[1] && h[5] == headerMagic[2] {
		return nil
	}

	return errors.New("Header magic mismatch ")
}
