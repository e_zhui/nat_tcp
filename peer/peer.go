package main

import (
	"../message"
	"encoding/hex"
	"errors"
	"fmt"
	"github.com/ethereum/go-ethereum/log"
	"golang.org/x/crypto/sha3"
	"gopkg.in/urfave/cli.v1"
	"io"
	"math/rand"
	"net"
	"os"
	"os/signal"
	"strings"
	"sync"
	"sync/atomic"
	"syscall"
	"time"
)

var (
	reConnectInterval = time.Second * 5
	readTimeout       = time.Second * 5
	writeTimeout      = time.Second * 5
)

type TCPPeer struct {
	ConnC  chan bool
	MsgC   chan *message.Msg
	ErrorC chan error

	port         string
	listen       net.Listener
	destAddr     string
	conn         net.Conn
	stop         chan chan bool
	quitConnect  chan chan bool
	connected    uint32
	isConnecting uint32
	isReading    uint32
	isAccepting  uint32

	rmu, wmu sync.Mutex
	rw       *message.StreamRW
}

func NewTCPPeer(destAddr string, port string) (*TCPPeer, error) {
	address := fmt.Sprintf("0.0.0.0:%s", port)

	log.Info("Listen on %s\n\r", address)
	listen, err := net.Listen("tcp", address)
	if err != nil {
		return nil, errors.New("")
	}

	client := &TCPPeer{
		ConnC:  make(chan bool),
		MsgC:   make(chan *message.Msg, 1),
		ErrorC: make(chan error),

		port:         port,
		listen:       listen,
		destAddr:     destAddr,
		connected:    0,
		isConnecting: 0,
		isReading:    0,
		isAccepting:  0,
		stop:         make(chan chan bool),
		quitConnect:  make(chan chan bool),
	}

	go client.AcceptLoop()
	go client.ConnectLoop()

	return client, nil
}

func (tc *TCPPeer) ConnectLoop() {
	if atomic.LoadUint32(&tc.isConnecting) == 1 {
		return
	}

	atomic.StoreUint32(&tc.isConnecting, 1)
	log.Info("Start connect loop")
out:
	for {
		select {
		case quitLoop := <-tc.quitConnect:
			quitLoop <- true
			break out
		case stop := <-tc.stop:
			stop <- true
			return
		default:
		}

		d := net.Dialer{Timeout: time.Second}
		c, err := d.Dial("tcp", tc.destAddr)
		if err != nil {
			log.Info("Fail to connect ", "error", err)
			time.Sleep(reConnectInterval)

			continue
		}

		// if connection is already established, do nothing but close this connection and quit ConnectLoop()
		if atomic.LoadUint32(&tc.connected) == 1 {
			c.Close()
			break out
		}

		// Tear down AcceptLoop() as we have connected to the remote peer
		atomic.StoreUint32(&tc.connected, 1)
		tc.listen.Close() // this will stop AcceptLoop()

		log.Info("Succeed to connect to remote server")
		tc.conn = c

		tc.rw = message.NewStreamRW(tc.conn)
		go tc.ReadLoop()
		tc.ConnC <- true

		break out
	}

	atomic.StoreUint32(&tc.isConnecting, 0)
	log.Info("Stop connect loop")
}

func (tc *TCPPeer) AcceptLoop() {
	if atomic.LoadUint32(&tc.isAccepting) == 1 {
		return
	}
	atomic.StoreUint32(&tc.isAccepting, 1)

	log.Info("Start accpet loop")
out:
	for {
		c, err := tc.listen.Accept()
		if err != nil {
			atomic.StoreUint32(&tc.isAccepting, 0)
			break out
		}

		// if connection is already established, do nothing but close this connection and quit ConnectLoop()
		if atomic.LoadUint32(&tc.connected) == 1 {
			c.Close()
			break out
		}

		atomic.StoreUint32(&tc.connected, 1)

		log.Info("Succeed to connect to remote peer")
		tc.conn = c

		tc.rw = message.NewStreamRW(tc.conn)
		go tc.ReadLoop()
		tc.ConnC <- true

		// Tear down ConnectLoop() as we have connected to the remote peer
		wait := make(chan bool)
		tc.quitConnect <- wait
		<-wait
	}

	log.Info("Stop accept loop")
}

func (tc *TCPPeer) ReadLoop() {
	if atomic.LoadUint32(&tc.isReading) == 1 {
		return
	}
	atomic.StoreUint32(&tc.isReading, 1)

	log.Trace("Start read loop.")

out:
	for {
		select {
		case stop := <-tc.stop:
			tc.conn.Close()
			atomic.StoreUint32(&tc.connected, 0)
			stop <- true
			return
		default:
		}

		// Check if peer is lost when peer closes the connection
		one := []byte{}
		tc.conn.SetReadDeadline(time.Now())
		if _, err := tc.conn.Read(one); err == io.EOF {
			tc.conn.Close()
			tc.ErrorC <- err
			break out
		}

		msg, err := tc.ReadMsg()
		if err != nil {
			netErr, ok := err.(net.Error)
			if ok && netErr.Timeout() {
				continue
			} else {
				tc.conn.Close()
				tc.ErrorC <- err
				break out
			}
		}
		// send message
		tc.MsgC <- msg
	}

	atomic.StoreUint32(&tc.isReading, 0)

	if atomic.LoadUint32(&tc.connected) == 1 {
		tc.conn.Close()
		atomic.StoreUint32(&tc.connected, 0)
	}
	log.Info("Stop read loop")
	go tc.ConnectLoop()
}

func (tc *TCPPeer) ReadMsg() (*message.Msg, error) {
	tc.rmu.Lock()
	defer tc.rmu.Unlock()
	tc.conn.SetReadDeadline(time.Now().Add(readTimeout))
	return tc.rw.ReadMsg()
}

func (tc *TCPPeer) WriteMsg(msg *message.Msg) error {
	tc.wmu.Lock()
	defer tc.wmu.Unlock()
	tc.conn.SetWriteDeadline(time.Now().Add(writeTimeout))
	return tc.rw.WriteMsg(msg)
}

func (tc *TCPPeer) Write(data []byte) (int, error) {
	if atomic.LoadUint32(&tc.connected) == 1 {
		return tc.conn.Write(data)
	} else {
		return 0, errors.New("Not connected ")
	}
}

func (tc *TCPPeer) Stop() {
	// Quit AcceptLoop()
	if atomic.LoadUint32(&tc.isAccepting) == 1 {
		tc.listen.Close()
	}

	// Stop ConnectLoop() or ReadLoop()
	join := make(chan bool)
	tc.stop <- join
	<-join

	log.Info("TCP peer stopped")
}

func handleMsg(client *TCPPeer) {
	userTerminate := make(chan os.Signal, 2)
	signal.Notify(userTerminate, os.Interrupt, syscall.SIGTERM)

out:
	for {
		select {
		case <-userTerminate:
			fmt.Println()
			client.Stop()

			break out
		case err := <-client.ErrorC:
			log.Info("TCP peer got error", err)
		case <-client.ConnC:
			log.Info("Connected to remote")
			log.Info("Send Ping message")
			client.WriteMsg(&message.Msg{Code: message.Ping})
		case msg := <-client.MsgC:
			log.Info("TCP peer got message %v", msg)

			if msg.Code == message.Ping {
				log.Info("Receive Ping message, send Pong message")
				client.WriteMsg(&message.Msg{Code: message.Pong})
			}

			if msg.Code == message.Pong {
				log.Info("Receive Pong message")
				time.Sleep(time.Second * 2)
				log.Info("Send Ping message")
				client.WriteMsg(&message.Msg{Code: message.Ping})
			}

		}
	}
}

func Hash(b []byte) []byte {
	h := make([]byte, 64)
	// Compute a 64-byte hash of buf and put it in h.
	sha3.ShakeSum256(h, b)

	return h
}

func getPeerAddr(c *cli.Context, identity []byte) (string, error) {
	bootAddr := c.String("boot")
	client, err := NewTCPPeer(bootAddr, fmt.Sprintf("%d", c.Int("lPort")))
	if err != nil {
		log.Error("Can not create TCP peer instance to connect to boot node.")
		return "", err
	}

	var (
		peerInfo string
	)

	quitGetInfo := make(chan bool, 1)
	getPeerInfo := func() {
		ticker := time.NewTicker(time.Second)
		for {
			select {
			case <-ticker.C:
				client.WriteMsg(&message.Msg{Code: message.GetPeerInfo})
			case <-quitGetInfo:
				ticker.Stop()
				return
			}
		}
	}

	userTerminate := make(chan os.Signal, 2)
	signal.Notify(userTerminate, os.Interrupt, syscall.SIGTERM)
out:
	for {
		select {
		case <-userTerminate:
			fmt.Println()
			client.Stop()

			return "", errors.New("User terminate ")
		case err := <-client.ErrorC:
			log.Error("TCP peer got error", err)
			quitGetInfo <- true
		case <-client.ConnC:
			log.Info("Connected to boot node")
			log.Info("Send Register message")
			client.WriteMsg(&message.Msg{Code: message.Register, Content: hex.EncodeToString(identity)})
			go getPeerInfo()
		case msg := <-client.MsgC:
			if msg.Code == message.PeerInfo {
				if len(msg.Content) != 0 {
					log.Info("TCP peer got peer info", "peer", msg.Content)
					peerInfo = msg.Content
					quitGetInfo <- true
					break out
				}
			} else {
				log.Info("TCP peer got unhandled message %v", msg)
			}
		}
	}

	client.Stop()

	return peerInfo, nil
}

func run(c *cli.Context) {
	glogger := log.NewGlogHandler(log.StreamHandler(os.Stderr, log.TerminalFormat(false)))
	glogger.Verbosity(log.Lvl(c.GlobalInt("log")))
	log.Root().SetHandler(glogger)

	log.Info("\n\r----------- Getting peer address, Press Ctrl+C to quit ----------- \n\r")

	rand.Seed(time.Now().UnixNano())
	buf, _ := hex.DecodeString(fmt.Sprintf("%d", rand.Intn(100)))
	identity := Hash(buf)

	peerInfo, err := getPeerAddr(c, identity)
	if err != nil {
		log.Error("Can not get peer address.")
		return
	}

	log.Info("Peer Info", "detail", peerInfo)
	//PeerInfo: PeerIP:PeerPort:MyPort
	info := strings.Split(peerInfo, ":")

	log.Info("\n\r----------- Try to create connection with remote, Press Ctrl+C to quit ----------- \n\r")
	//
	////remoteAddr := fmt.Sprintf("127.0.0.1:%d", c.Int("rPort"))
	client, err := NewTCPPeer(fmt.Sprintf("%s:%s", info[0], info[1]), fmt.Sprintf("%s", info[2]))
	if err != nil {
		log.Error("Can not create TCP peer instance.")
		return
	}

	handleMsg(client)
}

func main() {
	app := cli.NewApp()
	app.Name = "NatPeer"
	app.Usage = "Test NAT over TCP network"
	app.Action = run
	app.Flags = []cli.Flag{
		cli.IntFlag{
			Name:  "lPort",
			Usage: "Network listening port",
			Value: 30303,
		},
		cli.IntFlag{
			Name:  "rPort",
			Usage: "Network listening port",
			Value: 30304,
		},
		cli.StringFlag{
			Name:  "boot",
			Usage: "BootNode listening address and port",
			Value: "128.168.72.135:10000",
		},
		cli.IntFlag{
			Name:  "log",
			Usage: "BootNode log verbosity level, 1 : 6",
			Value: 6,
		},
	}
	err := app.Run(os.Args)
	if err != nil {
		log.Error("Fail to start ", err)
	}
}

//  ./peer --lPort 30304 --rPort 30303
