package main

import (
	"../message"
	"errors"
	"fmt"
	"github.com/ethereum/go-ethereum/log"
	"gopkg.in/urfave/cli.v1"
	"io"
	"net"
	"os"
	"os/signal"
	"strings"
	"sync"
	"syscall"
	"time"
)

var (
	readTimeout = time.Second
)

type Conn struct {
	fd         net.Conn
	rmu, wmu   sync.Mutex
	rw         *message.StreamRW
	id         string
	addr       string
	mappedPort string
	stop       chan chan bool
}

type Request struct {
	client *Conn
	msg    *message.Msg
}

type Boot struct {
	ConnC    chan string
	ErrorC   chan error
	RequestC chan *Request

	listen  net.Listener
	clients map[string]*Conn
}

func NewBoot(port int) (*Boot, error) {

	listenOn := fmt.Sprintf("0.0.0.0:%d", port)
	listen, err := net.Listen("tcp", listenOn)
	if err != nil {
		return nil, errors.New("")
	}

	log.Info("Start listening on: ", "addr", listenOn)

	boot := &Boot{
		ConnC:    make(chan string),
		ErrorC:   make(chan error),
		RequestC: make(chan *Request),

		listen:  listen,
		clients: make(map[string]*Conn, 0),
	}

	go boot.AcceptLoop()

	return boot, nil

}

func (b *Boot) AcceptLoop() {
out:
	for {
		c, err := b.listen.Accept()
		if err != nil {
			break out
		}

		log.Info("Accept new connection")
		addr := c.RemoteAddr().String()
		b.clients[addr] = &Conn{
			fd:   c,
			rw:   message.NewStreamRW(c),
			stop: make(chan chan bool),
		}

		go b.ReadLoop(b.clients[addr], addr)
		b.ConnC <- addr
	}

	log.Trace("Stop accept loop")
}

func (b *Boot) ReadLoop(client *Conn, addr string) {
	log.Trace("Start read on", "addr", addr)
out:
	for {

		// Check if remote connection is shutdown
		one := []byte{}
		client.fd.SetReadDeadline(time.Now())
		if _, err := client.fd.Read(one); err == io.EOF {
			log.Debug("Client %s dropped", addr)
			client.fd.Close()
			break out
		} else {
			client.fd.SetReadDeadline(time.Now().Add(readTimeout))
		}
		msg, err := client.rw.ReadMsg()

		//log.Trace("Read message returns", "error", err, "msg", msg)

		if err != nil {
			netErr, ok := err.(net.Error)
			if ok && netErr.Timeout() {
				//log.Trace("Read message timeout") // todo: never happens....
				continue
			} else {
				// send an error if it's encountered
				b.ErrorC <- err
				break out
			}
		}

		// Check if user stops ReadLoop()
		select {
		case stop := <-client.stop:
			client.fd.Close()
			stop <- true
			return
		default:
		}

		b.RequestC <- &Request{client, msg}
	}
	delete(b.clients, addr)

	log.Trace("Stop read on ", "addr", addr)
}

func (b *Boot) Stop() {
	b.listen.Close()
	// Stop ReadLoop() for each client
	for _, c := range b.clients {

		// Clear any pending request that may block the ReadLoop()
		select {
		case <-b.RequestC:
		default:
		}

		join := make(chan bool)
		c.stop <- join

		<-join
	}
}

func (b *Boot) GetPeerInfo(client *Conn) (string, error) {
	for _, c := range b.clients {
		if c.id != client.id {
			// returns peer's ip and port and it's own mapped port that boot node detects

			// PeerInfo: PeerIP:PeerPort:MyPort
			pInfo := fmt.Sprintf("%s:%s", c.addr, client.mappedPort)
			log.Info("Response to %s peer info: %s", client.addr, pInfo)

			return pInfo, nil
		}
	}

	return "", nil
}

func (b *Boot) RegisterClient(client *Conn, identity string) {
	client.id = identity
	client.addr = client.fd.RemoteAddr().String()

	s := strings.Split(client.addr, ":")
	client.mappedPort = s[1]
	log.Info("Client registration", "id", identity, "addr", client.addr)
}

func (b *Boot) HandleClientRequest(req *Request) error {
	client := req.client
	msg := req.msg

	switch {
	case msg.Code == message.Ping:
		return client.rw.WriteMsg(&message.Msg{Code: message.Pong})
	case msg.Code == message.Register:
		b.RegisterClient(client, msg.Content)
	case msg.Code == message.GetPeerInfo:
		if pInfo, err := b.GetPeerInfo(client); err == nil {
			return client.rw.WriteMsg(&message.Msg{Code: message.PeerInfo, Content: pInfo})
		}

	default:
		log.Trace("Unrecognized message code", "Code", msg.Code)
	}

	return nil
}

func handleMsg(boot *Boot) {
	userTerminate := make(chan os.Signal, 2)
	signal.Notify(userTerminate, os.Interrupt, syscall.SIGTERM)

out:
	for {
		select {
		case <-userTerminate:
			fmt.Println()
			boot.Stop()

			break out
		case err := <-boot.ErrorC:
			log.Debug("TCP peer got error", "error", err)
		case <-boot.ConnC:
			log.Debug("New client connected")
		case r := <-boot.RequestC:
			err := boot.HandleClientRequest(r)
			if err != nil {
				log.Debug("Fail to handle client request", "error", err)
			}
		}
	}
}

func runBoot(ctx *cli.Context) {
	glogger := log.NewGlogHandler(log.StreamHandler(os.Stderr, log.TerminalFormat(false)))
	glogger.Verbosity(log.Lvl(ctx.GlobalInt("log")))
	log.Root().SetHandler(glogger)

	log.Info("----------- TCP NAT Boot, Press Ctrl+C to quit ----------- ")

	boot, err := NewBoot(ctx.Int("port"))
	if err != nil {
		log.Debug("Can not create TCP peer instance.")
		return
	}

	handleMsg(boot)

}

func main() {
	app := cli.NewApp()
	app.Name = "Boot"
	app.Usage = "Test NAT over TCP network"
	app.Action = runBoot
	app.Flags = []cli.Flag{
		cli.IntFlag{
			Name:  "port",
			Usage: "BootNode listening port",
			Value: 10000,
		},
		cli.IntFlag{
			Name:  "log",
			Usage: "BootNode log verbosity level, 1 : 6",
			Value: 6,
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Error("Fail to start : ", err)
	}
}
